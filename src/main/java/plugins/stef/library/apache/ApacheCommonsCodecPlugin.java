package plugins.stef.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache Commons Codec library.
 * 
 * @author Stephane Dallongeville
 */
public class ApacheCommonsCodecPlugin extends Plugin implements PluginLibrary
{
    //
}
